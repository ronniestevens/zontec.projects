<?php
/**
 * Plugin Name:     Projects
 * Plugin URI:      https://bitbucket.org:ronniestevens/projects
 * Description:     Projects for Zontec
 * Author:          Ronnie Stevens
 * Author URI:      https://zoo.nl
 * Text Domain:     projects
 * Domain Path:     /languages
 * Version:         0.1.0
 *
 * @package         Projects
 */

// Your code starts here.



// Creates Projects Custom Post Type
function projecten_init() {
    $args = array(
        'label' => 'All Referenties',
        'singular_name' => 'referentie',
        'has_archive' => true,
        'public' => true,
        'show_ui' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'taxonomies' => 'category',
        'rewrite' => array('slug' => 'referenties'),
        'menu_name' => 'referenties',
        'query_var' => true,
        'menu_icon' => 'dashicons-video-alt',
        'show_in_rest' => true,
        'supports' => array(
            'title',
            'editor',
            'revisions',
            'thumbnail',
            'excerpt',
            'page-attributes',)
        );
    register_post_type( 'projecten', $args );
}
add_action( 'init', 'projecten_init' );

// function projects_meta_box_markup() {}

function add_projecten_meta_box() {
    add_meta_box(
        'projecten_meta_box', // $id
        'Projecten Velden', // $title
        'projecten_meta_box_markup', // $callback
        'projecten', // $screen
        'side', // $context
        'high', // $priority
        null
    );
}
add_action( 'add_meta_boxes', 'add_projecten_meta_box' );

function projecten_meta_box_markup($object){
    $meta = get_post_meta( $object->ID );
	$show_on_homepage = ( isset( $meta['show-on-homepage'][0] ) &&  '1' === $meta['show-on-homepage'][0] ) ? 1 : 0;
    wp_nonce_field(basename(__FILE__), "meta-box-nonce");

    ?>
        <div>
            <label for="meta-box-plaats">Plaats</label>
            <input name="meta-box-plaats" type="text" value="<?php echo get_post_meta($object->ID, "meta-box-plaats", true); ?>">

            <br>

            <label for="meta-box-soortbedrijf">Soort Bedrijf</label>
            <input name="meta-box-soortbedrijf" type="text" value="<?php echo get_post_meta($object->ID, "meta-box-soortbedrijf", true); ?>">

            <br>
            <label for="meta-box-omvang">Omvang</label>
            <input name="meta-box-omvang" type="text" value="<?php echo get_post_meta($object->ID, "meta-box-omvang", true); ?>">

            <br>
            <label for="meta-box-referentie">Referentie</label>
            <input name="meta-box-referentie" type="text" value="<?php echo get_post_meta($object->ID, "meta-box-referentie", true); ?>">
            <div>
			<label>
				Show on Homepage<br>
				<input type="checkbox" name="show-on-homepage" value="1" <?php checked( $show_on_homepage, 1 ); ?> />
			</label>	
		</div>
        </div>
    <?php  
}

function save_projecten_meta_box($post_id, $post, $update)
{
    if (!isset($_POST["meta-box-nonce"]) || !wp_verify_nonce($_POST["meta-box-nonce"], basename(__FILE__)))
        return $post_id;

    if(!current_user_can("edit_post", $post_id))
        return $post_id;

    if(defined("DOING_AUTOSAVE") && DOING_AUTOSAVE)
        return $post_id;

    $slug = "projecten";
    if($slug != $post->post_type)
        return $post_id;

    $meta_box_plaats_value = "";
    $meta_box_soortbedrijf_value = "";
    $meta_box_omvang_value = "";
    $meta_box_referentie_value = "";
    $meta_box_homepage_value = "";

    if(isset($_POST["meta-box-plaats"]))
    {
        $meta_box_plaats_value = $_POST["meta-box-plaats"];
    }   
    update_post_meta($post_id, "meta-box-plaats", $meta_box_plaats_value);

    if(isset($_POST["meta-box-soortbedrijf"]))
    {
        $meta_box_soortbedrijf_value = $_POST["meta-box-soortbedrijf"];
    }   
    update_post_meta($post_id, "meta-box-soortbedrijf", $meta_box_soortbedrijf_value);

    if(isset($_POST["meta-box-omvang"]))
    {
        $meta_box_omvang_value = $_POST["meta-box-omvang"];
    }   
    update_post_meta($post_id, "meta-box-omvang", $meta_box_omvang_value);

    if(isset($_POST["meta-box-referentie"])) {
        $meta_box_referentie_value = $_POST["meta-box-referentie"];
    }   
    update_post_meta($post_id, "meta-box-referentie", $meta_box_referentie_value);

    $show_on_homepage = ( isset( $_POST['show-on-homepage'] ) && '1' === $_POST['show-on-homepage'] ) ? 1 : 0;
	update_post_meta( $post_id, 'show-on-homepage', esc_attr( $show_on_homepage ) );
}

add_action("save_post", "save_projecten_meta_box", 10, 3);

// show in the admincolumn if an event is set for homepage
function add_homepage_column($page_columns) {
	$page_columns['show_on_homepage'] = 'Homepage';
	return $page_columns;
}

function display_homepage_column( $column_name, $id ) {
	if ( $column_name === 'show_on_homepage') {
		$hp = get_post_meta( get_the_ID(), 'show-on-homepage', true );
		if ($hp == 1) {
			echo 'Homepage';
		}	
	}
}
add_filter( 'manage_projecten_posts_columns', 'add_homepage_column' );
add_action( 'manage_projecten_posts_custom_column', 'display_homepage_column', 5, 2);

function add_projecten_order_column($projecten_order_columns) {
	$projecten_order_columns['menu_order'] = 'Order';
	return $projecten_order_columns;
}
add_filter( 'manage_projecten_posts_columns', 'add_projecten_order_column' );

function display_projecten_order_column( $name ) {
	global $post;

	switch($name) {
		case 'menu_order':
			$order = $post->menu_order;
			echo $order;
			break;
		default:
			break;
	}
}
add_action( 'manage_projecten_posts_custom_column', 'display_projecten_order_column', 5, 2);
